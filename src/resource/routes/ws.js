const io = require('../../lib/Socket');
const IbmWatson = require('../../lib/IBM/watson');

async function WsController(params, socket) {
  console.log(params);
  try {
    const ibmWatson = new IbmWatson();
    const session = params.sessionId || (await ibmWatson.createSession());

    const response = await ibmWatson.sendMessage(session, {
      contentMessage: params.message,
    });

    const data = {
      messages: response.result.output.generic,
      sessionId: session,
    };

    socket.emit('messages', data);
  } catch (error) {
    console.log(error);
  }
}

io.of('/whatsapp').on('connection', socket => {
  socket.on('messages', params => WsController(params, socket));
});

module.exports = io;
