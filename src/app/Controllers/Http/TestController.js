const IbmWatson = require('../../../lib/IBM/watson');

class TestController {
  async index(req, res) {
    const ibmWatson = new IbmWatson();

    const session = await ibmWatson.createSession();

    console.log(session);
    const response = await ibmWatson.sendMessage(session, {
      contentMessage: 'quanto tenho na minha conta',
    });

    const { result } = response;

    if (result) {
      console.log('result', result);
      console.log('result.context', result.context);
      console.log('result.context.global', result.context.global);
      console.log('result.context.skills', result.context.skills);
      console.log('generic: ', result.output.generic);
      console.log('intents: ', result.output.intents);
      console.log('sasentit ies: ', result.output.entities);
    }

    return res.status(200).json({ msg: 'Working' });
  }
}

module.exports = new TestController();
