const AssistantV2 = require('ibm-watson/assistant/v2');
const { IamAuthenticator } = require('ibm-watson/auth');

class Watson {
  constructor(params = {}) {
    this.assistantVersion =
      params.assistantVersion || process.env.IBM_WATSON_ASSISTANT_VERSION;

    this.assistantApiKey =
      params.assistantApiKey || process.env.IBM_WATSON_ASSISTANT_API_KEY;

    this.assistantApiUrl =
      params.assistantApiUrl || process.env.IBM_WATSON_ASSISTANT_URL;

    this.assistantId =
      params.assistantId || process.env.IBM_WATSON_ASSISTANT_ID;

    this.assistant = this.authentication();
  }

  authentication() {
    const assistant = new AssistantV2({
      version: this.assistantVersion,
      authenticator: new IamAuthenticator({
        apikey: this.assistantApiKey,
      }),
      url: this.assistantApiUrl,
    });

    return assistant;
  }

  async createSession() {
    try {
      const response = await this.assistant.createSession({
        assistantId: this.assistantId,
      });

      return response.result.session_id;
    } catch (error) {
      return false;
    }
  }

  deleteSession(sessionId) {
    try {
      this.assistant.deleteSession({
        assistantId: this.assistantId,
        sessionId,
      });
      return true;
    } catch (error) {
      return false;
    }
  }

  async sendMessage(sessionId, params = {}) {
    try {
      const response = await this.assistant.message({
        assistantId: this.assistantId,
        sessionId,
        input: {
          options: { return_context: true },
          message_type: params.messageType || 'text',
          text: params.contentMessage || null,
        },
        context: {
          global: {
            system: {
              timezone: params.timezone || 'GMT-03:00',
              user_id: this.convertToString(params.userId),
            },
          },
          skills: {
            'main skill': { user_defined: params.context || {} },
          },
        },
      });

      return response;
    } catch (error) {
      return false;
    }
  }

  convertToString(data) {
    if (typeof data === 'undefined') return 'NULL';
    if (typeof data === 'string') return data;
    return data.toString();
  }
}

module.exports = Watson;
